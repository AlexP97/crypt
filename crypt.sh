#!/bin/env bash

f_encrypt() {
    openssl aes-256-cbc -in $1 -out ./crypt-tmp -pass pass:$3
    dd of=$2 if=/dev/random bs=8 count=1 status=none
    dd of=$2 if=/dev/zero bs=8 count=31 seek=1 status=none
    cat ./crypt-tmp | wc -c | grep -oE '^[0-9]+' | dd of=$2 bs=8 seek=1 conv=notrunc status=none
    echo $(cat ./crypt-tmp | wc -c) bytes
    dd of=$2 if=./crypt-tmp bs=8 seek=32 conv=notrunc status=progress
    rm ./crypt-tmp
}

f_decrypt() {
    n="$(dd if=$1 bs=8 skip=1 count=31 status=none)"
    dd if=$1 of=./crypt-tmp skip=256 count=$n iflag=count_bytes,skip_bytes
    openssl aes-256-cbc -d -in ./crypt-tmp -out $2 -pass pass:$3
    rm ./crypt-tmp
}

case $1 in
    encrypt)
        f_encrypt $2 $3 $4
        ;;
    decrypt)
        f_decrypt $2 $3 $4
        ;;
    *)
    echo 'crypt <encrypt|decrupt> <in> <out> <pass>'
    ;;
esac
